﻿using System;
using System.IO;
using System.Text;

namespace SqlRunner.TempFileStoring
{
    /// <summary>
    /// Storage of temporary files that, when disposed, tries to delete its directory and created files
    /// </summary>
    public class TempFileStorage : ITempFileStorage
    {
        private readonly DirectoryInfo _directory;
        private bool _disposed;

        private TempFileStorage() : this(Path.GetTempPath()) {}

        private TempFileStorage(string parentDirPath) {
            do {
                string directoryName = Guid.NewGuid().ToString("N");
                _directory = new DirectoryInfo(Path.Combine(parentDirPath, directoryName));
            } while (_directory.Exists);
            _directory.Create();
        }

        #region ITempFileStorage Members

        public void Dispose() {
            Dispose(true);
        }

        public ITempFileBuilder NewTempFile {
            get {
                ThrowIfDisposed();
                return new TempFileBuilder(this);
            }
        }

        #endregion

        protected virtual void Dispose(bool disposing) {
            if (_disposed) {
                return;
            }
            _disposed = true;
            Cleanup();
        }

        private void Cleanup() {
            // ReSharper disable EmptyGeneralCatchClause
            try {
                foreach (FileInfo file in _directory.EnumerateFiles("*", SearchOption.AllDirectories)) {
                    try {
                        file.Delete();
                    }
                    catch (Exception) {} // continue even if any file could not be deleted
                }
            }
            catch (Exception) {} // continue even if any file could not be enumerated
            try {
                _directory.Delete(true);
            }
            catch (Exception) {} // continue even if the temp directory could not be deleted
            // ReSharper restore EmptyGeneralCatchClause
        }

        public static ITempFileStorage Create() {
            return new TempFileStorage();
        }

        private void ThrowIfDisposed() {
            if (_disposed) {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        #region Nested type: TempFileBuilder

        private class TempFileBuilder : ITempFileBuilder
        {
            private readonly TempFileStorage _storage;
            private string _contents;
            private Encoding _encoding = Encoding.Default;
            private string _extension = "tmp";
            private string _prefix = "";
            private string _relativeSubDirectoryPath = "";

            public TempFileBuilder(TempFileStorage storage) {
                _storage = storage;
            }

            #region ITempFileBuilder Members

            /// <summary>
            /// Defines the file's extension. By default, the file wil have the extension "tmp".
            /// </summary>
            /// <param name="extension">The file's extension, without the trailing ".".</param>
            /// <returns>The builder itself, to allow chained method calls.</returns>
            public ITempFileBuilder WithExtension(string extension) {
                _storage.ThrowIfDisposed();
                _extension = extension.TrimStart(new[] {'.'});
                return this;
            }

            /// <summary>
            /// Defines the prefix to be used in the generated file's name. By default, the file won't have a prefix.
            /// </summary>
            /// <param name="prefix">The file name's prefix.</param>
            /// <returns>The builder itself, to allow chained method calls.</returns>
            public ITempFileBuilder PrefixedBy(string prefix) {
                _storage.ThrowIfDisposed();
                _prefix = prefix;
                return this;
            }

            /// <summary>
            /// Defines the file's textual contents.
            /// </summary>
            /// <param name="contents">The file's textual contents</param>
            /// <returns>The builder itself, to allow chained method calls.</returns>
            public ITempFileBuilder Containing(string contents) {
                _storage.ThrowIfDisposed();
                _contents = contents;
                return this;
            }

            /// <summary>
            /// Defines the encoding to be used when writing the file's textual contents, if defined.
            /// </summary>
            /// <param name="encoding">The encoding for the file's contents.</param>
            /// <returns>The builder itself, to allow chained method calls.</returns>
            public ITempFileBuilder With(Encoding encoding) {
                _storage.ThrowIfDisposed();
                _encoding = encoding;
                return this;
            }

            /// <summary>
            /// Defines the path to the file, relative to the storage's internal root path. 
            /// It allows creating temporary files in a specific directory structure.
            /// </summary>
            /// <param name="relativeSubDirectoryPath"></param>
            /// <returns></returns>
            public ITempFileBuilder UnderSubDirectory(string relativeSubDirectoryPath) {
                _storage.ThrowIfDisposed();
                _relativeSubDirectoryPath = relativeSubDirectoryPath;
                return this;
            }

            /// <summary>
            /// Creates the temporary file, with a unused random name and the specified path, extension, contents and encoding, and returns its path.
            /// </summary>
            /// <returns>temporary file's path</returns>
            public string CreateFile() {
                _storage.ThrowIfDisposed();
                string filePath = CreateFileName();
                using (var stream = new StreamWriter(filePath, false, _encoding)) {
                    if (_contents != null) {
                        stream.Write(_contents);
                    }
                    stream.Close();
                }
                return filePath;
            }

            /// <summary>
            /// Generates and returns a unused random file name, with the specified path and extension.
            /// </summary>
            /// <returns>temporary file's path</returns>
            public string CreateFileName() {
                _storage.ThrowIfDisposed();
                string filePath;
                string fileDirPath = Path.Combine(_storage._directory.FullName, _relativeSubDirectoryPath);
                Directory.CreateDirectory(fileDirPath);
                do {
                    string fileName = _prefix + Guid.NewGuid().ToString("N") + '.' + (_extension ?? "tmp");
                    filePath = Path.Combine(fileDirPath, fileName);
                } while (File.Exists(filePath));

                return filePath;
            }

            #endregion
        }

        #endregion
    }
}