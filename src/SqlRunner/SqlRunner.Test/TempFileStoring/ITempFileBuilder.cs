using System.Text;

namespace SqlRunner.TempFileStoring
{
    public interface ITempFileBuilder
    {
        ITempFileBuilder WithExtension(string extension);
        ITempFileBuilder PrefixedBy(string prefix);
        ITempFileBuilder Containing(string contents);
        ITempFileBuilder With(Encoding encoding);
        ITempFileBuilder UnderSubDirectory(string relativeSubDirectoryPath);
        string CreateFile();
        string CreateFileName();
    }
}