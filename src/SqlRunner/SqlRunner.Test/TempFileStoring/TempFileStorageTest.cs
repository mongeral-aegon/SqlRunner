﻿using System;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace SqlRunner.TempFileStoring
{
    [TestFixture]
    public class TempFileStorageTest
    {
        private static bool DirectoryExists(string file1DirPath) {
            return Directory.Exists(file1DirPath);
        }

        [Test]
        public void CheckExpectedContentsSize() {
            using (var storage = TempFileStorage.Create()) {
                Assert.That(new FileInfo(storage.NewTempFile.CreateFile()).Length, Is.EqualTo(0));
                Assert.That(new FileInfo(storage.NewTempFile.Containing("ABC").With(Encoding.ASCII).CreateFile()).Length, Is.EqualTo(3));
                Assert.That(new FileInfo(storage.NewTempFile.Containing("ÃBC").With(Encoding.UTF8).CreateFile()).Length, Is.EqualTo(7));
                // 7 = 3 (BOM) + 2 (Ã) + 2 (BC) 
                Assert.That(new FileInfo(storage.NewTempFile.Containing("ÃBC").CreateFile()).Length,
                            Is.EqualTo(new FileInfo(storage.NewTempFile.Containing("ÃBC").With(Encoding.Default).CreateFile()).Length));
            }
        }

        [Test]
        public void CreateInSubDirectories() {
            string file1Path, file2Path, file3Path, file4Path, file5Path;
            string file1DirPath, file2DirPath, file3DirPath, file4DirPath, file5DirPath;
            using (var tempFileStorage = TempFileStorage.Create()) {
                file1Path = tempFileStorage.NewTempFile.UnderSubDirectory(@"dir1").CreateFile();
                file2Path = tempFileStorage.NewTempFile.UnderSubDirectory(@"dir1").CreateFile();
                file3Path = tempFileStorage.NewTempFile.UnderSubDirectory(@"dir2").CreateFile();
                file4Path = tempFileStorage.NewTempFile.UnderSubDirectory(@"dir2/dir21").CreateFile();
                file5Path = tempFileStorage.NewTempFile.UnderSubDirectory(@"dir2/dir22").CreateFile();

                file1DirPath = Path.GetDirectoryName(file1Path);
                file2DirPath = Path.GetDirectoryName(file2Path);
                file3DirPath = Path.GetDirectoryName(file3Path);
                file4DirPath = Path.GetDirectoryName(file4Path);
                file5DirPath = Path.GetDirectoryName(file5Path);

                // same dir
                Assert.That(file1DirPath, Is.EqualTo(file2DirPath));
                Assert.That(file3DirPath, Is.EqualTo(Path.GetDirectoryName(file4DirPath)));
                Assert.That(Path.GetDirectoryName(file4DirPath), Is.EqualTo(Path.GetDirectoryName(file5DirPath)));

                // different dirs
                Assert.That(file1DirPath, Is.Not.EqualTo(file3DirPath));
                Assert.That(file1DirPath, Is.Not.EqualTo(file4DirPath));
                Assert.That(file1DirPath, Is.Not.EqualTo(file5DirPath));
                Assert.That(file3DirPath, Is.Not.EqualTo(file4DirPath));
                Assert.That(file3DirPath, Is.Not.EqualTo(file5DirPath));
                Assert.That(file4DirPath, Is.Not.EqualTo(file5DirPath));
            }
            Assert.That(!File.Exists(file1Path));
            Assert.That(!File.Exists(file2Path));
            Assert.That(!File.Exists(file3Path));
            Assert.That(!File.Exists(file4Path));
            Assert.That(!File.Exists(file5Path));
            Assert.That(!DirectoryExists(file1DirPath));
            Assert.That(!DirectoryExists(file2DirPath));
            Assert.That(!DirectoryExists(file3DirPath));
            Assert.That(!DirectoryExists(file4DirPath));
            Assert.That(!DirectoryExists(file5DirPath));
        }

        [Test]
        public void CreateOnlyNamesWithExtensionsInSameDirectory() {
            using (var tempFileStorage = TempFileStorage.Create()) {
                string file1Path = tempFileStorage.NewTempFile.CreateFileName();
                string file2Path = tempFileStorage.NewTempFile.CreateFileName();
                string file3Path = tempFileStorage.NewTempFile.WithExtension("txt").CreateFileName();

                string file1DirPath = Path.GetDirectoryName(file1Path);
                string file2DirPath = Path.GetDirectoryName(file2Path);
                string file3DirPath = Path.GetDirectoryName(file3Path);

                Assert.That(file1Path, Is.Not.EqualTo(file2Path));
                Assert.That(file2Path, Is.Not.EqualTo(file3Path));
                Assert.That(file1Path, Is.Not.EqualTo(file3Path));

                Assert.That(file1Path.EndsWith(".tmp"));
                Assert.That(file2Path.EndsWith(".tmp"));
                Assert.That(file3Path.EndsWith(".txt"));

                Assert.That(file1DirPath, Is.EqualTo(file2DirPath));
                Assert.That(file1DirPath, Is.EqualTo(file3DirPath));

                Assert.That(!File.Exists(file1Path));
                Assert.That(!File.Exists(file2Path));
                Assert.That(!File.Exists(file3Path));
            }
        }

        [Test]
        public void CreateWithExtensionsInSameDirectory() {
            string file1Path, file2Path, file3Path;
            string file1DirPath, file2DirPath, file3DirPath;
            using (var tempFileStorage = TempFileStorage.Create()) {
                file1Path = tempFileStorage.NewTempFile.CreateFile();
                file2Path = tempFileStorage.NewTempFile.CreateFile();
                file3Path = tempFileStorage.NewTempFile.WithExtension("txt").CreateFile();

                file1DirPath = Path.GetDirectoryName(file1Path);
                file2DirPath = Path.GetDirectoryName(file2Path);
                file3DirPath = Path.GetDirectoryName(file3Path);

                Assert.That(file1Path, Is.Not.EqualTo(file2Path));
                Assert.That(file2Path, Is.Not.EqualTo(file3Path));
                Assert.That(file1Path, Is.Not.EqualTo(file3Path));

                Assert.That(file1Path.EndsWith(".tmp"));
                Assert.That(file2Path.EndsWith(".tmp"));
                Assert.That(file3Path.EndsWith(".txt"));

                Assert.That(file1DirPath, Is.EqualTo(file2DirPath));
                Assert.That(file1DirPath, Is.EqualTo(file3DirPath));

                Assert.That(File.Exists(file1Path));
                Assert.That(File.Exists(file2Path));
                Assert.That(File.Exists(file3Path));
            }
            Assert.That(!File.Exists(file1Path));
            Assert.That(!File.Exists(file2Path));
            Assert.That(!File.Exists(file3Path));
            Assert.That(!DirectoryExists(file1DirPath));
            Assert.That(!DirectoryExists(file2DirPath));
            Assert.That(!DirectoryExists(file3DirPath));
        }

        [Test]
        public void CreateWithPrefix() {
            using (var tempFileStorage = TempFileStorage.Create()) {
                Assert.That(Path.GetFileName(tempFileStorage.NewTempFile.PrefixedBy("ABC").CreateFile()), Is.StringStarting("ABC"));
                Assert.That(Path.GetFileName(tempFileStorage.NewTempFile.PrefixedBy("ABC").CreateFileName()), Is.StringStarting("ABC"));
                Assert.That(Path.GetFileName(tempFileStorage.NewTempFile.PrefixedBy("DEF").CreateFile()), Is.StringStarting("DEF"));
                Assert.That(Path.GetFileName(tempFileStorage.NewTempFile.PrefixedBy("DEF").CreateFileName()), Is.StringStarting("DEF"));
            }
        }

        [Test]
        public void DirectoryAlreadyDeletedBeforeDispose() {
            using (var tempFileStorage = TempFileStorage.Create()) {
                var file1Path = tempFileStorage.NewTempFile.CreateFile();
                var file1DirPath = Path.GetDirectoryName(file1Path);
                if (file1DirPath != null) {
                    Directory.Delete(file1DirPath, true);
                }
            }
        }

        [Test]
        public void DisposeTwiceDoesNotCleanUpAgain() {
            var tempFileStorage = TempFileStorage.Create();
            var filePath = tempFileStorage.NewTempFile.CreateFile();
            tempFileStorage.Dispose();
            Assert.That(!File.Exists(filePath));
            var fileDirPath = Path.GetDirectoryName(filePath);
            if (fileDirPath != null) {
                Directory.CreateDirectory(fileDirPath);
            }
            File.CreateText(filePath).Close();
            Assert.That(File.Exists(filePath));
            tempFileStorage.Dispose();
            Assert.That(File.Exists(filePath));
        }

        [Test]
        public void FileAlreadyDeletedBeforeDispose() {
            string file1Path, file2Path, file3Path;
            using (var tempFileStorage = TempFileStorage.Create()) {
                file1Path = tempFileStorage.NewTempFile.CreateFile();
                file2Path = tempFileStorage.NewTempFile.CreateFile();
                file3Path = tempFileStorage.NewTempFile.CreateFile();
                Assert.That(File.Exists(file1Path));
                Assert.That(File.Exists(file2Path));
                Assert.That(File.Exists(file3Path));
                File.Delete(file2Path);
                Assert.That(!File.Exists(file2Path));
            }
            Assert.That(!File.Exists(file1Path));
            Assert.That(!File.Exists(file2Path));
            Assert.That(!File.Exists(file3Path));
        }

        [Test]
        public void FileInUseWhileDisposing() {
            string file1Path, file2Path, file3Path;
            FileStream fileBeingRead;
            using (ITempFileStorage tempFileStorage = TempFileStorage.Create()) {
                file1Path = tempFileStorage.NewTempFile.CreateFile();
                file2Path = tempFileStorage.NewTempFile.CreateFile();
                file3Path = tempFileStorage.NewTempFile.CreateFile();
                Assert.That(File.Exists(file1Path));
                Assert.That(File.Exists(file2Path));
                Assert.That(File.Exists(file3Path));
                fileBeingRead = File.OpenRead(file2Path);
                Assert.That(fileBeingRead, Is.Not.Null);
            }
            Assert.That(!File.Exists(file1Path));
            Assert.That(File.Exists(file2Path)); // not deleted, because file is in use
            Assert.That(!File.Exists(file3Path));
            fileBeingRead.Close();
            File.Delete(file2Path);
            Assert.That(!File.Exists(file2Path));
        }

        [Test]
        public void ThrowExceptionWhenUsedAfterDisposal() {
            ITempFileStorage tempFileStorage = TempFileStorage.Create();
            tempFileStorage.NewTempFile.CreateFile();
            tempFileStorage.Dispose();
            Assert.Throws<ObjectDisposedException>(() => tempFileStorage.NewTempFile.CreateFile());
        }
    }
}