using System;

namespace SqlRunner.TempFileStoring
{
    public interface ITempFileStorage : IDisposable
    {
        /// <summary>
        /// Returns a temporary file builder, that allows to specify aspects of the temporary file being created (subdirectory, extension, contents, encoding etc.).
        /// Note that the file is created only when the method CreateFile is called.
        /// </summary>
        /// <returns>temporary file builder</returns>
        /// <example>
        /// This sample shows how to get the file builder and use it to create a temporary file.
        /// <code>
        /// using (var storage = TempFileStorage.CreateFile()) {
        ///     string file1Path = storage.NewTempFile.CreateFile();
        ///     ...
        /// }
        /// </code>
        /// </example>
        ITempFileBuilder NewTempFile { get; }
    }
}