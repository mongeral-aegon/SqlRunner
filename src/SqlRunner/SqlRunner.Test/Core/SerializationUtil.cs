using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SqlRunner.Core
{
    public static class SerializationUtil
    {
        public static T BinarySerializeDeserialize<T>(T input) {
            var stream = new MemoryStream(1024);
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, input);
            Debug.WriteLine(stream.Position);
            stream.Position = 0;
            return (T) formatter.Deserialize(stream);
        }
    }
}