﻿using System;
using System.Globalization;
using NUnit.Framework;
using SqlRunner.TempFileStoring;

namespace SqlRunner.Core
{
    [TestFixture]
    public class SqlFileTest
    {
        [Test]
        public void Create() {
            using (ITempFileStorage storage = TempFileStorage.Create()) {
                string filePath1 = storage.NewTempFile.Containing("ABC").CreateFile();
                var sqlFile1 = new SqlFile(filePath1);
                Assert.That(sqlFile1.Info.FullName, Is.EqualTo(filePath1));
                Assert.That(sqlFile1.Hash, Is.Not.Null);
                Assert.That(sqlFile1.Info.Exists);

                string filePath2 = storage.NewTempFile.Containing("ABC").CreateFile();
                var sqlFile2 = new SqlFile(filePath2);
                Assert.That(sqlFile2.Info.FullName, Is.EqualTo(filePath2));
                Assert.That(sqlFile2.Hash, Is.Not.Null);

                string filePath3 = storage.NewTempFile.Containing("DEF").CreateFile();
                var sqlFile3 = new SqlFile(filePath3);
                Assert.That(sqlFile3.Info.FullName, Is.EqualTo(filePath3));
                Assert.That(sqlFile3.Hash, Is.Not.Null);

                Assert.That(sqlFile1.Hash, Is.EqualTo(sqlFile2.Hash));
                Assert.That(sqlFile1.Hash, Is.Not.EqualTo(sqlFile3.Hash));
                Assert.That(sqlFile2.Hash, Is.Not.EqualTo(sqlFile3.Hash));
            }
        }

        [Test]
        public void CreateUsingCustomHashCalculator() {
            Func<SqlFile, string> hashCalculator = (file => file.Info.FullName.GetHashCode().ToString(CultureInfo.InvariantCulture));
            const string filePath1 = @"C:\abcde";
            var sqlFile = new SqlFile(filePath1, hashCalculator);
            Assert.That(sqlFile.Hash, Is.EqualTo(hashCalculator(sqlFile)));
        }

        [Test]
        public void NullArgumentThrowsException() {
            Assert.Throws<ArgumentNullException>(delegate { new SqlFile(null, null); });
            Assert.Throws<ArgumentNullException>(delegate { new SqlFile(String.Empty, null); });
        }

        [Test]
        public void SqlFileThatDoesNotExistReturnsNullHash() {
            var sqlFile = new SqlFile(@"C:\abcde");
            Assert.That(!sqlFile.Info.Exists);
            Assert.That(sqlFile.Hash, Is.EqualTo(string.Empty));
        }
    }
}