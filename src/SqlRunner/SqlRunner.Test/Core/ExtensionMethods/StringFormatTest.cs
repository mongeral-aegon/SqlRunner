﻿using NUnit.Framework;

namespace SqlRunner.Core.ExtensionMethods
{
    [TestFixture]
    public class StringFormatTest
    {
        [TestCase("A{0}{1}{2}", new object[] {"B", "C", "D"})]
        public void FormatSelf(string format, params object[] args) {
            Assert.That(format.FormatSelf(args), Is.EqualTo(string.Format(format, args)));
        }
    }
}