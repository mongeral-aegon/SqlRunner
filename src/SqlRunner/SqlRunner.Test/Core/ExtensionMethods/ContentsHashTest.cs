﻿using System;
using System.IO;
using NUnit.Framework;
using SqlRunner.TempFileStoring;

namespace SqlRunner.Core.ExtensionMethods
{
    [TestFixture]
    public class ContentsHashTest
    {
        [Test]
        public void ContentsHash() {
            using (var storage = TempFileStorage.Create()) {
                var file1 = new FileInfo(storage.NewTempFile.Containing("ABC").CreateFile());
                var file2 = new FileInfo(storage.NewTempFile.Containing("ABC").CreateFile());
                var file3 = new FileInfo(storage.NewTempFile.Containing("DEF").CreateFile());
                Assert.That(file1.ContentsHash(), Is.EqualTo(file2.ContentsHash()));
                Assert.That(file1.ContentsHash(), Is.Not.EqualTo(file3.ContentsHash()));
                Assert.That(file2.ContentsHash(), Is.Not.EqualTo(file3.ContentsHash()));
            }
        }

        [Test]
        public void NullArgumentThrowsException() {
            Assert.Throws<ArgumentNullException>(() => ((FileInfo) null).ContentsHash());
        }
    }
}