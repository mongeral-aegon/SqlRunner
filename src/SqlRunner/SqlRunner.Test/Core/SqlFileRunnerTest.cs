﻿using System;
using System.IO;
using NUnit.Framework;

namespace SqlRunner.Core
{
    [TestFixture]
    public class SqlFileRunnerTest
    {
        private readonly string _anySupportedProviderName = GetAnySupportedProviderName();

        private static string GetAnySupportedProviderName() {
            var supportedDataProviders = DataProviderUtil.ListSupported();
            return supportedDataProviders.Count == 0 ? null : supportedDataProviders[0].InvariantName;
        }

        private class SqlFileReaderThatThrows<T> : ISqlFileReader where T : Exception, new()
        {
            #region ISqlFileReader Members

            public void ReadRunningSqlCommands(Stream stream, long streamTotalBytes, Action<string, long, long> sqlExecutor) {
                throw new T();
            }

            #endregion
        }

        private static bool RunSqlCommands(DatabaseTestUtil.TestParameters testParameters) {
            using (var sqlFileRunner = new SqlFileRunner(testParameters.ConnectionString, testParameters.DataProviderName)) {
                sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand1FilePath_CreateTable));
                sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand2FilePath_Insert1));
                sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand3FilePath_Insert2));
            }
            return true;
        }

        private static bool RunSqlCommandsThrowingExceptions(DatabaseTestUtil.TestParameters testParameters) {
            using (var sqlFileRunner = new SqlFileRunner(testParameters.ConnectionString, testParameters.DataProviderName)) {
                sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand1FilePath_CreateTable));
                sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand2FilePath_Insert1));
                sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand3FilePath_Insert2));

                Assert.Throws<SqlFileRunningException>(() => sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand1FilePath_CreateTable)));
                    // table already exists
                sqlFileRunner.SqlFileReader = new SqlFileReaderThatThrows<UnauthorizedAccessException>();
                Assert.Throws<SqlFileRunningException>(() => sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand3FilePath_Insert2)));
                sqlFileRunner.SqlFileReader = new SqlFileReaderThatThrows<IOException>();
                Assert.Throws<SqlFileRunningException>(() => sqlFileRunner.Run(new SqlFile(testParameters.SqlCommand3FilePath_Insert2)));
            }
            return true;
        }

        [Test]
        public void CanHandleExceptions() {
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(RunSqlCommandsThrowingExceptions, true);
        }

        [Test]
        public void DisposeTwice() {
            var sqlFileRunner = new SqlFileRunner("", _anySupportedProviderName);
            sqlFileRunner.Dispose();
            sqlFileRunner.Dispose();
        }

        [Test]
        public void NullArgumentThrowsException() {
            var sqlFileRunner = new SqlFileRunner("", _anySupportedProviderName);
            Assert.Throws<ArgumentNullException>(() => sqlFileRunner.Run(null));
        }

        [Test]
        public void RunSqlCommands() {
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(RunSqlCommands, true);
        }

        [Test]
        public void ThrowExceptionWhenUsedAfterDisposal() {
            var sqlFileRunner = new SqlFileRunner("", _anySupportedProviderName);
            sqlFileRunner.Dispose();
            Assert.Throws<ObjectDisposedException>(() => sqlFileRunner.Run(null));
        }
    }
}