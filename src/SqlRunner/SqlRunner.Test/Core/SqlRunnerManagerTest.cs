﻿using System;
using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace SqlRunner.Core
{
    [TestFixture]
    public class SqlRunnerManagerTest
    {
        public class SqlFileRunnerResult
        {
            public bool Value;
        }

        [TestCase("Normal")]
        [TestCase("Simulation")]
        [TestCase("UpdateVersionOnly")]
        public void RunFindingTwoFilesWhichOnlySecondOneIsOutdatedAndShouldBeExecuted(string modeName) {
            Mode mode;
            Mode.TryParse(modeName, out mode);
            const string file1Path = @"c:\f1.sql";
            const string file2Path = @"c:\f2.sql";
            const string file3Path = @"c:\f3.sql";

            var mockFileFinder = new Mock<IFileFinder>();
            var mockFileVersionDao = new Mock<IFileVersionDao>();
            var mockSqlFileRunner = new Mock<ISqlFileRunner>();

            // all three files will be found, but not in this order
            mockFileFinder.Setup(x => x.Find()).Returns(new[] {file2Path, file1Path, file3Path});

            // all three files are known as having version 'fakeHashVersion1'
            mockFileVersionDao.Setup(x => x.Find(It.IsAny<string>())).Returns("fakeHashVersion1");
            var savedVersions = new Dictionary<string, string>();
            mockFileVersionDao.Setup(x => x.Save(It.IsAny<string>(), It.IsAny<string>())).Callback<string, string>(savedVersions.Add);

            // report 'fakeHashVersion2' as the current version of second and third files
            Func<SqlFile, string> hashCalculator = (file => file.Info.FullName == file1Path ? "fakeHashVersion1" : "fakeHashVersion2");

            var processedFiles = new List<SqlFile>();
            mockSqlFileRunner.Setup(x => x.Run(It.IsAny<SqlFile>())).Callback<SqlFile>(processedFiles.Add);

            var sqlFileVersionManager = new SqlFileVersionManager(mockFileVersionDao.Object, hashCalculator);
            using (var sqlRunnerManager = new SqlRunnerManager(sqlFileVersionManager, mockFileFinder.Object, mockSqlFileRunner.Object)
                ) {
                sqlRunnerManager.Run();
            }

            // verify that only the third and second file (outdated), in this order, was processed
            Assert.That(processedFiles.Count, Is.EqualTo(2));
            Assert.That(processedFiles[0].Info.FullName, Is.EqualTo(file2Path));
            Assert.That(processedFiles[1].Info.FullName, Is.EqualTo(file3Path));

            // verify the saved file versions
            Assert.That(savedVersions[file2Path], Is.EqualTo(hashCalculator(new SqlFile(file2Path))));
            Assert.That(savedVersions[file3Path], Is.EqualTo(hashCalculator(new SqlFile(file3Path))));
            Assert.That(!savedVersions.ContainsKey(file1Path)); // in the test, this file is reported as being up to date, so it won't be neither run nor saved.
        }

        [Test]
        public void DisposeTwice() {
            var mockSqlFileVersionManager = new Mock<ISqlFileVersionManager>();
            var mockFileFinder = new Mock<IFileFinder>();
            var mockSqlFileRunner = new Mock<ISqlFileRunner>();
            var sqlRunnerManager = new SqlRunnerManager(mockSqlFileVersionManager.Object, mockFileFinder.Object, mockSqlFileRunner.Object);
            sqlRunnerManager.Dispose();
            sqlRunnerManager.Dispose();
        }

        [Test]
        public void NullArgumentThrowsException() {
            var mockSqlFileVersionManager = new Mock<ISqlFileVersionManager>();
            var mockFileFinder = new Mock<IFileFinder>();
            var mockSqlFileRunner = new Mock<ISqlFileRunner>();

            Assert.Throws<ArgumentNullException>(delegate { new SqlRunnerManager(null, mockFileFinder.Object, mockSqlFileRunner.Object); });
            Assert.Throws<ArgumentNullException>(delegate { new SqlRunnerManager(mockSqlFileVersionManager.Object, null, mockSqlFileRunner.Object); });
            Assert.Throws<ArgumentNullException>(delegate { new SqlRunnerManager(mockSqlFileVersionManager.Object, mockFileFinder.Object, null); });
        }

        [Test]
        public void RunningAFileContainingErrorsShouldAbortTheProcess() {
            const string file1Path = @"c:\f1.sql";
            const string file2Path = @"c:\f2.sql";
            const string file3Path = @"c:\f3.sql";

            var mockFileFinder = new Mock<IFileFinder>();
            var mockFileVersionDao = new Mock<IFileVersionDao>();
            var mockSqlFileRunner = new Mock<ISqlFileRunner>();

            // all three files will be found, but not in this order
            mockFileFinder.Setup(x => x.Find()).Returns(new[] {file2Path, file1Path, file3Path});

            // all three files are known as having version 'fakeHashVersion1'
            mockFileVersionDao.Setup(x => x.Find(It.IsAny<string>())).Returns("fakeHashVersion1");
            var savedVersions = new Dictionary<string, string>();
            mockFileVersionDao.Setup(x => x.Save(It.IsAny<string>(), It.IsAny<string>())).Callback<string, string>(savedVersions.Add);

            // report 'fakeHashVersion2' as the current version of all files
            Func<SqlFile, string> hashCalculator = (file => "fakeHashVersion2");

            // These return values mean that the only the first file will cause an error
            var processedFiles = new List<SqlFile>();
            mockSqlFileRunner.Setup(x => x.Run(It.IsAny<SqlFile>())).Callback(delegate(SqlFile file) {
                                                                                  processedFiles.Add(file);
                                                                                  mockSqlFileRunner.Setup(x => x.Run(It.IsAny<SqlFile>())).Throws
                                                                                      <SqlFileRunningException>();
                                                                              });

            var sqlFileVersionManager = new SqlFileVersionManager(mockFileVersionDao.Object, hashCalculator);

            using (var sqlRunnerManager = new SqlRunnerManager(sqlFileVersionManager, mockFileFinder.Object, mockSqlFileRunner.Object)) {
                sqlRunnerManager.Run();
            }

            // verify that only the third and second file (outdated), in this order, was executed
            Assert.That(processedFiles.Count, Is.EqualTo(1));
            Assert.That(processedFiles[0].Info.FullName, Is.EqualTo(file1Path));

            // verify the saved file versions
            Assert.That(savedVersions[file1Path], Is.EqualTo(hashCalculator(new SqlFile(file1Path))));
            Assert.That(!savedVersions.ContainsKey(file2Path)); // this file was processed, but not saved, due to simulated error 
            Assert.That(!savedVersions.ContainsKey(file3Path)); // this file was not even processed, due to error in previous file
        }

        [Test]
        public void ThrowExceptionWhenUsedAfterDisposal() {
            var mockSqlFileVersionManager = new Mock<ISqlFileVersionManager>();
            var mockFileFinder = new Mock<IFileFinder>();
            var mockSqlFileRunner = new Mock<ISqlFileRunner>();

            var sqlRunnerManager = new SqlRunnerManager(mockSqlFileVersionManager.Object, mockFileFinder.Object, mockSqlFileRunner.Object);
            sqlRunnerManager.Dispose();
            Assert.Throws<ObjectDisposedException>(sqlRunnerManager.Run);
        }
    }
}