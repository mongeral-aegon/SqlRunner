﻿using System;
using NUnit.Framework;

namespace SqlRunner.Core
{
    [TestFixture]
    public class SimulationSqlFileRunnerTest
    {
        [Test]
        public void ThrowExceptionWhenRunWithNullArgument() {
            var sqlFileRunner = new SimulationSqlFileRunner();
            Assert.Throws<ArgumentNullException>(() => sqlFileRunner.Run(null));
        }
    }
}