﻿using System;
using Moq;
using NUnit.Framework;

namespace SqlRunner.Core
{
    [TestFixture]
    public class SqlFileVersionManagerTest
    {
        [Test]
        public void NullArgumentThrowsException() {
            var mockFileVersionDao = new Mock<IFileVersionDao>();
            var sqlFileVersionManager = new SqlFileVersionManager(mockFileVersionDao.Object, null);
            Assert.Throws<ArgumentNullException>(delegate { sqlFileVersionManager.FileVersionIsOutdated(null); });
            Assert.Throws<ArgumentNullException>(delegate { sqlFileVersionManager.Save(null); });
        }
    }
}