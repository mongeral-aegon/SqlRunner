﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace SqlRunner.Core
{
    [TestFixture]
    public class SqlFileReaderTest
    {
        [TestCase("select 1", new[] {"select 1"})]
        [TestCase("select 1;select 2", new[] {"select 1", "select 2"})]
        [TestCase("select 1 ; select 2", new[] {"select 1", "select 2"})]
        [TestCase("select 1, ';';select 2", new[] {"select 1, ';'", "select 2"})]
        [TestCase("select 1, ''';';select 2", new[] {"select 1, ''';'", "select 2"})]
        [TestCase("select 1, '''\n;\n'\n;\nselect 2", new[] {"select 1, '''\n;\n'", "select 2"})]
        [TestCase("select 1, '\";';select 2", new[] {"select 1, '\";'", "select 2"})]
        [TestCase("select 1, '\"\n;\n'\n;\nselect 2", new[] {"select 1, '\"\n;\n'", "select 2"})]
        public void Read(string sql, string[] expectedSqlCommands) {
            var sqlCommands = new List<string>();
            byte[] sqlBytes = Encoding.GetEncoding(1252).GetBytes(sql);
            var sqlFileReader = new SqlFileReader();
            using (var stream = new MemoryStream(sqlBytes)) {
                sqlFileReader.ReadRunningSqlCommands(stream, sqlBytes.Length, (sqlCommand, processedBytes, totalBytes) => sqlCommands.Add(sqlCommand));
            }
            CollectionAssert.AreEquivalent(expectedSqlCommands, sqlCommands);
        }

        [Test]
        public void NullArgumentThrowsException() {
            var sqlFileReader = new SqlFileReader();
            Assert.Throws<ArgumentNullException>(() => sqlFileReader.ReadRunningSqlCommands(null, 0, null));
            Assert.Throws<ArgumentNullException>(() => sqlFileReader.ReadRunningSqlCommands(new MemoryStream(new byte[0]), 0, null));
        }
    }
}