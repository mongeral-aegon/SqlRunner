﻿using System;
using System.Text;
using NUnit.Framework;

namespace SqlRunner.Core
{
    [TestFixture]
    public class LoggerTest
    {
        [Test]
        public void LogToNoListeners() {
            Logger.Log(this, "A");
        }

        [Test]
        public void LogToTwoListeners() {
            var stringBuilder1 = new StringBuilder();
            var stringBuilder2 = new StringBuilder();
            EventHandler<LoggedEventArgs> loggedEventHandler1 = (sender, loggedEventArgs) => stringBuilder1.Append(loggedEventArgs.Message);
            EventHandler<LoggedEventArgs> loggedEventHandler2 = (sender, loggedEventArgs) => stringBuilder2.Append(loggedEventArgs.Message);
            try {
                Logger.Logged += loggedEventHandler1;
                Logger.Logged += loggedEventHandler2;
                Logger.Log(this, "A");
                Logger.Log(this, "B");
                Logger.Log(this, "C");
                Assert.That(stringBuilder1.ToString(), Is.EqualTo("ABC"));
                Assert.That(stringBuilder2.ToString(), Is.EqualTo("ABC"));
            }
            finally {
                Logger.Logged -= loggedEventHandler1;
                Logger.Logged -= loggedEventHandler2;
            }
        }
    }
}