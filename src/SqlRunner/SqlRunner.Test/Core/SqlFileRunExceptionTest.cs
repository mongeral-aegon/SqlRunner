﻿using NUnit.Framework;

namespace SqlRunner.Core
{
    [TestFixture]
    public class SqlFileRunExceptionTest
    {
        [Test]
        public void CanBeConstructed() {
            Assert.That(new SqlFileRunningException("message").Message, Is.EqualTo("message"));
        }

        [Test]
        public void CanBeConstructedByDeserialization() {
            var exception = new SqlFileRunningException("message");
            Assert.That(SerializationUtil.BinarySerializeDeserialize(exception).ToString(), Is.EqualTo(exception.ToString()));
        }
    }
}