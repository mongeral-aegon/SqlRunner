﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using SqlRunner.TempFileStoring;

namespace SqlRunner.Core
{
    [TestFixture]
    public class FileFinderTest
    {
        [Test]
        public void CreateWithNullDirectories() {
            Assert.Throws<ArgumentNullException>(() => new FileFinder(null));
        }

        [Test]
        public void CreateWithNullDirectories2() {
            Assert.Throws<ArgumentNullException>(() => new FileFinder(null, "*"));
        }

        [Test]
        public void CreateWithNullPattern() {
            Assert.Throws<ArgumentNullException>(() => new FileFinder(new[] {"abc"}, null));
        }

        [Test]
        public void Scan() {
            using (ITempFileStorage storage1 = TempFileStorage.Create()) {
                using (ITempFileStorage storage2 = TempFileStorage.Create()) {
                    var filePaths = new[] {
                                              storage1.NewTempFile.CreateFile(),
                                              storage1.NewTempFile.WithExtension("txt").CreateFile(),
                                              storage1.NewTempFile.UnderSubDirectory("dir1").CreateFile(),
                                              storage2.NewTempFile.CreateFile(),
                                              storage2.NewTempFile.WithExtension("txt").CreateFile(),
                                              storage2.NewTempFile.UnderSubDirectory("dir1").CreateFile()
                                          };
                    var txtFilePaths = new[] {
                                                 filePaths[1],
                                                 filePaths[4]
                                             };
                    string storage1Path = Path.GetDirectoryName(filePaths[0]);
                    string storage2Path = Path.GetDirectoryName(filePaths[3]);

                    var storageDirectories = new[] {
                                                       storage1Path,
                                                       storage2Path
                                                   };

                    var fileFinder = new FileFinder(storageDirectories);
                    CollectionAssert.AreEquivalent(filePaths, fileFinder.Find().ToList());

                    var txtFileFinder = new FileFinder(storageDirectories, "*.txt");
                    CollectionAssert.AreEquivalent(txtFilePaths, txtFileFinder.Find().ToList());
                }
            }
        }
    }
}