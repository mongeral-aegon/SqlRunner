﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Moq;
using NUnit.Framework;
using SqlRunner.TempFileStoring;

namespace SqlRunner.Core
{
    [TestFixture]
    public class FileSystemFileVersionDaoTest
    {
        #region Setup/Teardown

        [SetUp]
        public void Init() {
            _storage = TempFileStorage.Create();
        }

        [TearDown]
        public void CleanUp() {
            _storage.Dispose();
        }

        #endregion

        private ITempFileStorage _storage;

        [Test]
        public void CanHandleDeserializationExceptions() {
            var mockSerializer = new Mock<IXmlSerializer>();
            string controlFilePath;

            controlFilePath = _storage.NewTempFile.CreateFile();
            mockSerializer.Setup(x => x.Deserialize(It.IsAny<Stream>())).Throws<SerializationException>();
            Assert.Throws<InvalidOperationException>(() => new FileSystemFileVersionDao(controlFilePath, mockSerializer.Object));

            controlFilePath = _storage.NewTempFile.CreateFile();
            mockSerializer.Setup(x => x.Deserialize(It.IsAny<Stream>())).Throws<IOException>();
            Assert.Throws<InvalidOperationException>(() => new FileSystemFileVersionDao(controlFilePath, mockSerializer.Object));

            controlFilePath = _storage.NewTempFile.CreateFile();
            mockSerializer.Setup(x => x.Deserialize(It.IsAny<Stream>())).Throws<UnauthorizedAccessException>();
            Assert.Throws<InvalidOperationException>(() => new FileSystemFileVersionDao(controlFilePath, mockSerializer.Object));
        }

        [Test]
        public void CanHandleSerializationExceptions() {
            var mockSerializer = new Mock<IXmlSerializer>();
            string controlFilePath;

            controlFilePath = _storage.NewTempFile.CreateFile();
            File.Delete(controlFilePath); // to force dao recreate the file
            mockSerializer.Setup(x => x.Serialize(It.IsAny<Stream>(), It.IsAny<object>())).Throws<SerializationException>();
            Assert.Throws<InvalidOperationException>(() => new FileSystemFileVersionDao(controlFilePath, mockSerializer.Object));

            controlFilePath = _storage.NewTempFile.CreateFile();
            File.Delete(controlFilePath); // to force dao recreate the file
            mockSerializer.Setup(x => x.Serialize(It.IsAny<Stream>(), It.IsAny<object>())).Throws<IOException>();
            Assert.Throws<InvalidOperationException>(() => new FileSystemFileVersionDao(controlFilePath, mockSerializer.Object));

            controlFilePath = _storage.NewTempFile.CreateFile();
            File.Delete(controlFilePath); // to force dao recreate the file
            mockSerializer.Setup(x => x.Serialize(It.IsAny<Stream>(), It.IsAny<object>())).Throws<UnauthorizedAccessException>();
            Assert.Throws<InvalidOperationException>(() => new FileSystemFileVersionDao(controlFilePath, mockSerializer.Object));
        }

        [Test]
        public void GetUnknownFile() {
            string controlFilePath = _storage.NewTempFile.CreateFile();
            File.Delete(controlFilePath); // to force dao recreate the file
            var fileVersionDao = new FileSystemFileVersionDao(controlFilePath);
            Assert.That(fileVersionDao.Find("C:\abcde.txt"), Is.Null);
        }

        [Test]
        public void LoadAnEmptyFileThrowsException() {
            string controlFilePath = _storage.NewTempFile.CreateFile();
            Assert.Throws<InvalidOperationException>(() => new FileSystemFileVersionDao(controlFilePath));
        }

        [Test]
        public void LoadPreviouslySavedControlFile() {
            const string fileId1 = "C:\test.txt";
            const string fileVersion1 = "ABC";
            string controlFilePath = _storage.NewTempFile.CreateFile();
            File.Delete(controlFilePath); // to force dao recreate the file
            var fileVersionDao1 = new FileSystemFileVersionDao(controlFilePath);
            fileVersionDao1.Save(fileId1, fileVersion1);
            Assert.That(fileVersionDao1.Find(fileId1), Is.EqualTo(fileVersion1));

            const string fileId2 = "C:\test2.txt";
            const string fileVersion2 = "DEF";
            var fileVersionDao2 = new FileSystemFileVersionDao(controlFilePath);
            Assert.That(fileVersionDao2.Find(fileId1), Is.EqualTo(fileVersion1));
            fileVersionDao2.Save(fileId2, fileVersion2);

            var fileVersionDao3 = new FileSystemFileVersionDao(controlFilePath);
            Assert.That(fileVersionDao3.Find(fileId1), Is.EqualTo(fileVersion1));
            Assert.That(fileVersionDao3.Find(fileId2), Is.EqualTo(fileVersion2));
        }

        [Test]
        public void SaveAndGet() {
            const string fileId = "C:\test.txt";
            const string fileVersion = "ABC";
            string controlFilePath = _storage.NewTempFile.CreateFile();
            File.Delete(controlFilePath); // to force dao recreate the file
            var fileVersionDao = new FileSystemFileVersionDao(controlFilePath);
            fileVersionDao.Save(fileId, fileVersion);
            Assert.That(fileVersionDao.Find(fileId), Is.EqualTo(fileVersion));
        }
    }
}