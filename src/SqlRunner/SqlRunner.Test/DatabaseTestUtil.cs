﻿using System;
using System.Data.SqlServerCe;
using NUnit.Framework;
using SqlRunner.Core;
using SqlRunner.TempFileStoring;

namespace SqlRunner
{
    public static class DatabaseTestUtil
    {
        public static void RunTestAgainstTemporarySqlCeDb(Func<TestParameters, bool> testAction, bool updatesDatabase) {
            using (ITempFileStorage tempFileStorage = TempFileStorage.Create()) {
                var controlFilePath = tempFileStorage.NewTempFile.WithExtension("xml").CreateFileName();
                var dbFilePath = tempFileStorage.NewTempFile.WithExtension("sdf").CreateFileName();
                var connectionString = "Data Source={0}".FormatSelf(dbFilePath);
                using (var sqlCeEngine = new SqlCeEngine(connectionString)) {
                    sqlCeEngine.CreateDatabase();

                    ITempFileBuilder tempFileBuilder = tempFileStorage.NewTempFile.WithExtension("sql");
                    string sqlCommand1FilePath = tempFileBuilder.PrefixedBy("1").Containing(
                        "CREATE TABLE [Table1] ([Id] [int] IDENTITY(1,1) NOT NULL, [Text] [nvarchar](50) NOT NULL)").CreateFile();
                    string sqlCommand2FilePath = tempFileBuilder.PrefixedBy("2").Containing(
                        "INSERT INTO [Table1] ([Text]) VALUES ('abc')").CreateFile();
                    string sqlCommand3FilePath = tempFileBuilder.PrefixedBy("3").Containing(
                        "INSERT INTO [Table1] ([Text]) VALUES ('def')").CreateFile();
                    const string dataProviderName = "System.Data.SqlServerCe.4.0";
                    var testParameters = new TestParameters {
                                                                ConnectionString = connectionString,
                                                                DataProviderName = dataProviderName,
                                                                ControlFilePath = controlFilePath,
                                                                SqlCommand1FilePath_CreateTable = sqlCommand1FilePath,
                                                                SqlCommand2FilePath_Insert1 = sqlCommand2FilePath,
                                                                SqlCommand3FilePath_Insert2 = sqlCommand3FilePath
                                                            };
                    if (!testAction(testParameters)) {
                        return;
                    }

                    using (var conn = new SqlCeConnection(connectionString)) {
                        conn.Open();
                        if (!updatesDatabase) {
                            using (var cmd = new SqlCeCommand("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Table1'", conn)) {
                                Assert.That(cmd.ExecuteScalar(), Is.EqualTo(0));
                            }
                        } else {
                            using (var cmd = new SqlCeCommand("SELECT [Id], [Text] FROM [Table1] ORDER BY [Id]", conn)) {
                                SqlCeDataReader dataReader = cmd.ExecuteReader();
                                Assert.That(dataReader.Read());
                                Assert.That(dataReader.GetSqlInt32(0).Value, Is.EqualTo(1));
                                Assert.That(dataReader.GetString(1), Is.EqualTo("abc"));
                                Assert.That(dataReader.Read());
                                Assert.That(dataReader.GetSqlInt32(0).Value, Is.EqualTo(2));
                                Assert.That(dataReader.GetString(1), Is.EqualTo("def"));
                                Assert.That(dataReader.Read(), Is.False);
                            }
                        }
                    }
                }
            }
        }

        #region Nested type: TestParameters

        public struct TestParameters
        {
            // ReSharper disable InconsistentNaming
            public string ConnectionString;
            public string ControlFilePath;
            public string DataProviderName;
            public string SqlCommand1FilePath_CreateTable;
            public string SqlCommand2FilePath_Insert1;
            public string SqlCommand3FilePath_Insert2;
            // ReSharper restore InconsistentNaming
        }

        #endregion
    }
}