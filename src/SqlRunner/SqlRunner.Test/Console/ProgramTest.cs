﻿using System;
using System.IO;
using NUnit.Framework;
using SqlRunner.Core;

namespace SqlRunner.Console
{
    [TestFixture]
    public class ProgramTest
    {
        private static bool RunConsoleApplicationOutProcess(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" \"-connectionString:{2}\" -dataProviderName:{3}"
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath,
                            testParameters.ConnectionString,
                            testParameters.DataProviderName);
            var result = new ConsoleAppRunner().RunConsoleApplicationOutProcess("SqlRunner.exe", arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.SuccessExitCode));
            return true;
        }

        private static bool RunConsoleApplicationInProcess(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" \"-connectionString:{2}\" -dataProviderName:{3}"
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath,
                            testParameters.ConnectionString,
                            testParameters.DataProviderName);
            var result = new ConsoleAppRunner().RunConsoleApplicationInProcess(SqlRunnerConsoleApp.AppMain, arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.SuccessExitCode));
            return true;
        }

        private static bool RunConsoleApplicationOutProcessInUpdateVersionOnlyMode(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" -mode:UpdateVersionOnly"
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath);
            var result = new ConsoleAppRunner().RunConsoleApplicationOutProcess("SqlRunner.exe", arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.SuccessExitCode));
            return true;
        }

        private static bool RunConsoleApplicationOutProcessInSimulationMode(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" -mode:Simulation"
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath);
            var result = new ConsoleAppRunner().RunConsoleApplicationOutProcess("SqlRunner.exe", arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.SuccessExitCode));
            return true;
        }

        private static bool RunConsoleApplicationInProcessInUpdateVersionOnlyMode(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" -mode:UpdateVersionOnly "
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath);
            var result = new ConsoleAppRunner().RunConsoleApplicationInProcess(SqlRunnerConsoleApp.AppMain, arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.SuccessExitCode));
            return true;
        }

        private static bool RunConsoleApplicationInProcessInSimulationMode(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" -mode:Simulation "
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath);
            var result = new ConsoleAppRunner().RunConsoleApplicationInProcess(SqlRunnerConsoleApp.AppMain, arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.SuccessExitCode));
            return true;
        }

        private static bool RunConsoleApplicationInProcessWithNoArguments(DatabaseTestUtil.TestParameters testParameters) {
            var result = new ConsoleAppRunner().RunConsoleApplicationInProcess(SqlRunnerConsoleApp.AppMain, "");
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.SuccessExitCode));
            return false;
        }

        private static bool RunConsoleApplicationInProcessWithInvalidArguments(DatabaseTestUtil.TestParameters testParameters) {
            var result = new ConsoleAppRunner().RunConsoleApplicationInProcess(SqlRunnerConsoleApp.AppMain, "-xyz xyz");
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.ErrorExitCode));
            return false;
        }

        private static bool RunConsoleApplicationInProcessWithInvalidDataProviderNameArgument(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" \"-connectionString:{2}\" -dataProviderName:{3}"
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath,
                            testParameters.ConnectionString,
                            "FAKE_INVALID_DATA_PROVIDER_NAME");
            var result = new ConsoleAppRunner().RunConsoleApplicationInProcess(SqlRunnerConsoleApp.AppMain, arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.ErrorExitCode));
            return false;
        }

        private static bool RunConsoleApplicationInProcessWithInvalidModeArgument(DatabaseTestUtil.TestParameters testParameters) {
            var arguments = "\"-searchDir:{0}\" \"-controlFile:{1}\" \"-connectionString:{2}\" -dataProviderName:{3} -mode:{4}"
                .FormatSelf(Path.GetDirectoryName(testParameters.SqlCommand1FilePath_CreateTable), testParameters.ControlFilePath,
                            testParameters.ConnectionString,
                            testParameters.DataProviderName, "FAKE_INVALID_MODE");
            var result = new ConsoleAppRunner().RunConsoleApplicationInProcess(SqlRunnerConsoleApp.AppMain, arguments);
            System.Console.WriteLine(result.StandardOutput);
            Assert.That(result.ExitCode, Is.EqualTo(SqlRunnerConsoleApp.ErrorExitCode));
            return false;
        }


        [TestCase("Normal")]
        [TestCase("Simulation")]
        [TestCase("UpdateVersionOnly")]
        public void RunConsoleApplicationInProcess(string modeName) {
            Mode mode;
            Mode.TryParse(modeName, out mode);
            Func<DatabaseTestUtil.TestParameters, bool> testBodyDelegate;
            if (mode == Mode.UpdateVersionOnly) {
                testBodyDelegate = RunConsoleApplicationInProcessInUpdateVersionOnlyMode;
            } else if (mode == Mode.Simulation) {
                testBodyDelegate = RunConsoleApplicationInProcessInSimulationMode;
            } else {
                // Normal
                testBodyDelegate = RunConsoleApplicationInProcess;
            }
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(testBodyDelegate, mode.ExecuteSql);
        }

        [TestCase("Normal")]
        [TestCase("Simulation")]
        [TestCase("UpdateVersionOnly")]
        public void RunConsoleApplicationOutProcess(string modeName) {
            Mode mode;
            Mode.TryParse(modeName, out mode);
            Func<DatabaseTestUtil.TestParameters, bool> testBodyDelegate;
            if (mode == Mode.UpdateVersionOnly) {
                testBodyDelegate = RunConsoleApplicationOutProcessInUpdateVersionOnlyMode;
            } else if (mode == Mode.Simulation) {
                testBodyDelegate = RunConsoleApplicationOutProcessInSimulationMode;
            } else {
                // Normal
                testBodyDelegate = RunConsoleApplicationOutProcess;
            }
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(testBodyDelegate, mode.ExecuteSql);
        }

        [Test]
        public void RunConsoleApplicationInProcessWithInvalidArguments() {
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(RunConsoleApplicationInProcessWithInvalidArguments, true);
        }

        [Test]
        public void RunConsoleApplicationInProcessWithInvalidDataProviderNameArgument() {
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(RunConsoleApplicationInProcessWithInvalidDataProviderNameArgument, true);
        }

        [Test]
        public void RunConsoleApplicationInProcessWithInvalidModeArgument() {
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(RunConsoleApplicationInProcessWithInvalidModeArgument, true);
        }

        [Test]
        public void RunConsoleApplicationInProcessWithNoArguments() {
            DatabaseTestUtil.RunTestAgainstTemporarySqlCeDb(RunConsoleApplicationInProcessWithNoArguments, true);
        }
    }
}