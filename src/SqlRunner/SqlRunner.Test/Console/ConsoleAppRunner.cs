using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace SqlRunner.Console
{
    /// <summary>
    /// Based on the code from article "How to Test Console Applications" http://www.codeproject.com/KB/dotnet/Console_Application_Test.aspx
    /// </summary>
    public class ConsoleAppRunner
    {
        private const char StringDelimiterCharacter = '"';
        private const char ArgumentSeparatorCharacter = ' ';

        private readonly StringBuilder _capturingStringBuilder;
        private readonly StringWriter _capturingWriter;
        private readonly string _executingAssemblyDirectoryPath;
        private TextWriter _bakConsoleOut;

        public ConsoleAppRunner() {
            // Set current folder to testing folder
            _executingAssemblyDirectoryPath = GetExecutingAssemblyDirectoryPath();
            _capturingStringBuilder = new StringBuilder();
            _capturingWriter = new StringWriter(_capturingStringBuilder);
        }

        public static string GetExecutingAssemblyDirectoryPath() {
            string assemblyCodeBase = Assembly.GetExecutingAssembly().CodeBase;
            string executingAssemblyDirectoryPath = Path.GetDirectoryName(assemblyCodeBase);

            if (executingAssemblyDirectoryPath != null && executingAssemblyDirectoryPath.StartsWith("file:\\")) {
                executingAssemblyDirectoryPath = executingAssemblyDirectoryPath.Substring(6);
            }
            return executingAssemblyDirectoryPath;
        }

        public ExecutionResult RunConsoleApplicationOutProcess(string executableFileName, string arguments) {
            _bakConsoleOut = System.Console.Out;
            System.Console.SetOut(_capturingWriter);
            var bakCurrentDirectory = Environment.CurrentDirectory;
            Environment.CurrentDirectory = _executingAssemblyDirectoryPath;
            try {
                var proc = new Process {
                                           StartInfo = {
                                                           FileName = executableFileName,
                                                           Arguments = arguments,
                                                           UseShellExecute = false,
                                                           RedirectStandardOutput = true,
                                                           RedirectStandardError = true,
                                                           CreateNoWindow = true
                                                       }
                                       };
                proc.Start();
                proc.WaitForExit(30000);
                System.Console.WriteLine("Standard Output:");
                System.Console.WriteLine(proc.StandardOutput.ReadToEnd());
                System.Console.WriteLine("Standard Error:");
                System.Console.WriteLine(proc.StandardError.ReadToEnd());
                return new ExecutionResult {
                                               ExitCode = proc.ExitCode,
                                               StandardOutput = _capturingStringBuilder.ToString()
                                           };
            }
            finally {
                _capturingStringBuilder.Clear();
                Environment.CurrentDirectory = bakCurrentDirectory;
                System.Console.SetOut(_bakConsoleOut);
            }
        }

        public ExecutionResult RunConsoleApplicationInProcess(Func<string[], byte> mainMethod, string arguments) {
            _bakConsoleOut = System.Console.Out;
            System.Console.SetOut(_capturingWriter);
            string bakCurrentDirectory = Environment.CurrentDirectory;
            Environment.CurrentDirectory = _executingAssemblyDirectoryPath;
            try {
                int exitCode = mainMethod(ParseCommandLineArguments(arguments));
                return new ExecutionResult {
                                               ExitCode = exitCode,
                                               StandardOutput = _capturingStringBuilder.ToString()
                                           };
            }
            finally {
                _capturingStringBuilder.Clear();
                Environment.CurrentDirectory = bakCurrentDirectory;
                System.Console.SetOut(_bakConsoleOut);
            }
        }

        public string[] ParseCommandLineArguments(string arguments) {
            var isInsideDelimitedString = false;
            var parsedArguments = new List<string>();
            var argumentBuilder = new StringBuilder();
            foreach (char character in arguments) {
                if (character == StringDelimiterCharacter) {
                    isInsideDelimitedString = !isInsideDelimitedString;
                    argumentBuilder.Append(character);
                } else {
                    if (character == ArgumentSeparatorCharacter && !isInsideDelimitedString) {
                        AddParsedCommandLineArgument(parsedArguments, argumentBuilder);
                    } else {
                        argumentBuilder.Append(character);
                    }
                }
            }
            AddParsedCommandLineArgument(parsedArguments, argumentBuilder);
            return parsedArguments.ToArray();
        }

        private static void AddParsedCommandLineArgument(List<string> parsedArguments, StringBuilder argumentBuilder) {
            string parsedArgument = argumentBuilder.ToString().Trim().Trim(new[] {StringDelimiterCharacter});
            argumentBuilder.Clear();
            if (parsedArgument.Length > 0) {
                parsedArguments.Add(parsedArgument);
            }
        }

        #region Nested type: ExecutionResult

        public struct ExecutionResult
        {
            public int ExitCode;
            public string StandardOutput;
        }

        #endregion
    }
}