﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("SqlRunner Core Library")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mongeral Aegon Seguros e Previdência S/A")]
[assembly: AssemblyProduct("SqlRunner")]
[assembly: AssemblyCopyright("Copyright © 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("67f84b48-44d8-4ffd-beb8-ac78f2bdd2c2")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: CLSCompliant(true)]
[assembly:
    InternalsVisibleTo(
        "SqlRunner.Test, PublicKey=0024000004800000940000000602000000240000525341310004000001000100350cf1816ad8f5069947bfbde1009e7e6df84108e2f3c40a1b6bcbe6f15013b7698f325198198cbdde0989cb246d777fef226b1e83b045cf5c6263937a61cf1035512d6d28cbc50e880b1d36bae348d5e27f389b3b356f1e8e0dc46c8075ca982bc251e6cbe8366949700105108b352b982436df270c9fa807dc9cda453716b7"
        )]