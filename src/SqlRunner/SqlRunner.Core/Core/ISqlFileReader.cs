using System;
using System.IO;

namespace SqlRunner.Core
{
    public interface ISqlFileReader
    {
        void ReadRunningSqlCommands(Stream stream, long streamTotalBytes, Action<string, long, long> sqlExecutor);
    }
}