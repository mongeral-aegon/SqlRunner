namespace SqlRunner.Core
{
    public class SupportedDataProvider
    {
        public string FriendlyName { get; internal set; }
        public string InvariantName { get; internal set; }
    }
}