using System;

namespace SqlRunner.Core
{
    public class SqlFileVersionManager : ISqlFileVersionManager
    {
        private readonly IFileVersionDao _fileVersionDao;
        private readonly Func<SqlFile, string> _hashCalculator;

        public SqlFileVersionManager(IFileVersionDao fileVersionDao) : this(fileVersionDao, null) {}

        public SqlFileVersionManager(IFileVersionDao fileVersionDao, Func<SqlFile, string> hashCalculator) {
            _fileVersionDao = fileVersionDao;
            _hashCalculator = hashCalculator;
        }

        #region ISqlFileVersionManager Members

        public Func<SqlFile, string> HashCalculator {
            get { return _hashCalculator; }
        }

        public bool FileVersionIsOutdated(SqlFile sqlFile) {
            if (sqlFile == null) {
                throw new ArgumentNullException("sqlFile");
            }
            var sqlFileKnownVersion = sqlFile.Hash;
            var sqlFileId = GetFileId(sqlFile.Info.FullName);
            var sqlFileCurrentVersion = _fileVersionDao.Find(sqlFileId);
            return sqlFileKnownVersion != sqlFileCurrentVersion;
        }

        public void Save(SqlFile file) {
            if (file == null) {
                throw new ArgumentNullException("file");
            }
            _fileVersionDao.Save(GetFileId(file.Info.FullName), file.Hash);
        }

        #endregion

        private static string GetFileId(string filePath) {
            return filePath;
        }
    }
}