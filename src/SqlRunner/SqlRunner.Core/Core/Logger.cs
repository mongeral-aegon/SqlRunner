﻿using System;

namespace SqlRunner.Core
{
    /// <summary>
    /// Simple static event-based logging implementation.
    /// For the sake of simplicity, the project does not reference any IoC or logging framework. 
    /// So, this implementation allows simple logging from any class of the project without any configuration.
    /// In the future, it can be replaced by a more robust solution.
    /// </summary>
    public static class Logger
    {
        public static event EventHandler<LoggedEventArgs> Logged;

        public static void Log(object sender, string message) {
            var handler = Logged;
            if (handler != null) {
                handler(sender, new LoggedEventArgs(message));
            }
        }

        public static void ClearEventHandlers() {
            Logged = null;
        }
    }
}