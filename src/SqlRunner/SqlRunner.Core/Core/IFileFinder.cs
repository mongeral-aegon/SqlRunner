using System.Collections.Generic;

namespace SqlRunner.Core
{
    /// <summary>
    /// Finds files in a file system, using some predefined criteria.
    /// </summary>
    public interface IFileFinder
    {
        IEnumerable<string> Find();
    }
}