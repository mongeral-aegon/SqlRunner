using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;

namespace SqlRunner.Core
{
    public static class ExtensionMethods
    {
        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters",
            Justification =
                "This extension method is only capable to operate on files, not directories. For this reason, the specific class FileInfo is being used, instead of its base class FileSystemInfo, which could represent directories."
            )]
        public static string ContentsHash(this FileInfo fileInfo) {
            if (fileInfo == null) {
                throw new ArgumentNullException("fileInfo");
            }
            using (HashAlgorithm hashAlg = new SHA1Managed()) {
                using (Stream file = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read)) {
                    byte[] hash = hashAlg.ComputeHash(file);
                    return BitConverter.ToString(hash);
                }
            }
        }

        public static string FormatSelf(this string str, params object[] args) {
            return string.Format(CultureInfo.CurrentCulture, str, args);
        }
    }
}