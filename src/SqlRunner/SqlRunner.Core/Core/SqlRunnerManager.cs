using System;
using System.Collections.Generic;
using System.Linq;

namespace SqlRunner.Core
{
    public sealed class SqlRunnerManager : IDisposable
    {
        private readonly IFileFinder _fileFinder;
        private readonly ISqlFileRunner _sqlFileRunner;
        private readonly ISqlFileVersionManager _sqlFileVersionManager;
        private bool _disposed;


        public SqlRunnerManager(ISqlFileVersionManager sqlFileVersionManager, IFileFinder fileFinder, ISqlFileRunner sqlFileRunner) {
            if (sqlFileVersionManager == null) {
                throw new ArgumentNullException("sqlFileVersionManager");
            }
            if (fileFinder == null) {
                throw new ArgumentNullException("fileFinder");
            }
            if (sqlFileRunner == null) {
                throw new ArgumentNullException("sqlFileRunner");
            }

            _fileFinder = fileFinder;
            _sqlFileRunner = sqlFileRunner;
            _sqlFileVersionManager = sqlFileVersionManager;
        }

        #region IDisposable Members

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        public void Run() {
            if (_disposed) {
                throw new ObjectDisposedException(GetType().Name);
            }
            var filesToRun = SearchSqlFilesToRun();
            var sortedFilesToRun = filesToRun.OrderBy(sqlFile => sqlFile.Info.FullName).ToList();
            try {
                var numberOfFilesToRun = ((IList<SqlFile>) sortedFilesToRun).Count;
                for (var i = 0; i < numberOfFilesToRun; i++) {
                    var currentFile = ((IList<SqlFile>) sortedFilesToRun)[i];
                    Logger.Log(this, "Running SQL script {0} of {1}.".FormatSelf(i + 1, numberOfFilesToRun));
                    try {
                        _sqlFileRunner.Run(currentFile);
                        _sqlFileVersionManager.Save(currentFile);
                    }
                    catch (SqlFileRunningException) {
                        Logger.Log(this, "Aborted! {0} of {1} SQL scripts were successfullly processed.".FormatSelf(i, numberOfFilesToRun));
                        throw;
                    }
                }
            }
            catch (SqlFileRunningException) {
                return;
            }
            Logger.Log(this, "Done! {0} SQL scripts were successfullly processed.".FormatSelf(sortedFilesToRun.Count));
        }

        private IEnumerable<SqlFile> SearchSqlFilesToRun() {
            Logger.Log(this, "Searching for SQL scripts to run...");
            var allFiles = _fileFinder.Find().ToList();
            var filesToRun = (IList<SqlFile>) allFiles
                                                  .Select(
                                                      file =>
                                                      _sqlFileVersionManager.HashCalculator == null
                                                          ? new SqlFile(file)
                                                          : new SqlFile(file, _sqlFileVersionManager.HashCalculator))
                                                  .Where(_sqlFileVersionManager.FileVersionIsOutdated)
                                                  .ToList();
            Logger.Log(this, "Searching complete. {0} SQL scripts were found, {1} of them are outdated.".FormatSelf(allFiles.Count, filesToRun.Count));
            return filesToRun;
        }

        private void Dispose(bool disposing) {
            if (_disposed) {
                return;
            }
            if (disposing) {
                if (_sqlFileRunner != null) {
                    _sqlFileRunner.Dispose();
                }
            }
            _disposed = true;
        }
    }
}