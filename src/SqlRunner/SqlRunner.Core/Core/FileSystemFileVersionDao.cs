using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;

namespace SqlRunner.Core
{
    public interface IXmlSerializer
    {
        object Deserialize(Stream inputStream);
        void Serialize(Stream outputStream, object source);
    }

    public class XmlSerializer : IXmlSerializer
    {
        private readonly System.Xml.Serialization.XmlSerializer _xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof (List<DataItem>));

        #region IXmlSerializer Members

        public object Deserialize(Stream inputStream) {
            return _xmlSerializer.Deserialize(inputStream);
        }

        public void Serialize(Stream outputStream, object source) {
            _xmlSerializer.Serialize(outputStream, source);
        }

        #endregion
    }

    /// <summary>
    /// Implementation of <see cref="IFileVersionDao"/> that uses a XML file as the persistence storage.
    /// During contruction, it loads the entries from the file, or initialize the file if it does not exist.
    /// Then it saves all the entries in the file every time a file's version is saved.
    /// </summary>
    public class FileSystemFileVersionDao : MemoryFileVersionDao
    {
        private readonly string _controlFilePath;
        private readonly IXmlSerializer _serializer;

        public FileSystemFileVersionDao(string controlFilePath) : this(controlFilePath, new XmlSerializer()) {}

        public FileSystemFileVersionDao(string controlFilePath, IXmlSerializer serializer) {
            _serializer = serializer;
            _controlFilePath = controlFilePath;
            FileVersions.Clear();
            if (!File.Exists(_controlFilePath)) {
                SaveControlFile();
            } else {
                RunHandlingSerializationExceptions("Error loading control file '{0}'.", _controlFilePath, delegate {
                                                                                                              using (
                                                                                                                  var readStream =
                                                                                                                      File.OpenRead(_controlFilePath)) {
                                                                                                                  var dataItems =
                                                                                                                      (List<DataItem>)
                                                                                                                      _serializer.Deserialize(readStream);
                                                                                                                  foreach (var dataItem in dataItems) {
                                                                                                                      FileVersions[dataItem.Key] =
                                                                                                                          dataItem.Value;
                                                                                                                  }
                                                                                                              }
                                                                                                          });
            }
        }

        protected override void PersistData() {
            base.PersistData();
            SaveControlFile();
        }

        private void SaveControlFile() {
            var outputData = FileVersions.Select(outputEntry => new DataItem(outputEntry.Key, outputEntry.Value)).ToList();
            RunHandlingSerializationExceptions("Error loading control file '{0}'.", _controlFilePath, delegate {
                                                                                                          using (
                                                                                                              var writeStream = File.OpenWrite(_controlFilePath)
                                                                                                              ) {
                                                                                                              _serializer.Serialize(writeStream, outputData);
                                                                                                          }
                                                                                                      });
        }

        private static void RunHandlingSerializationExceptions(string message, string currentFilePath, Action action) {
            Func<Exception, InvalidOperationException> encapsulateException =
                exception => new InvalidOperationException(message.FormatSelf(currentFilePath), exception);
            try {
                action();
            }
            catch (UnauthorizedAccessException e) {
                throw encapsulateException(e);
            }
            catch (IOException e) {
                throw encapsulateException(e);
            }
            catch (SerializationException e) {
                throw encapsulateException(e);
            }
        }
    }
}