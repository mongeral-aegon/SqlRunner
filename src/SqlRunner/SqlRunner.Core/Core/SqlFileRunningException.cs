using System;
using System.Runtime.Serialization;

namespace SqlRunner.Core
{
    [Serializable]
    public sealed class SqlFileRunningException : Exception
    {
        public SqlFileRunningException() {}
        public SqlFileRunningException(string message) : base(message) {}
        public SqlFileRunningException(string message, Exception innerException) : base(message, innerException) {}
        private SqlFileRunningException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }
}