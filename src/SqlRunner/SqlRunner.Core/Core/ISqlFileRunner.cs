using System;

namespace SqlRunner.Core
{
    /// <summary>
    /// Run the SQL commands contained in a SQL file.
    /// </summary>
    public interface ISqlFileRunner : IDisposable
    {
        /// <summary>
        /// Run the SQL commands contained in the specified <see cref="SqlFile"/>.
        /// </summary>
        /// <param name="sqlFile">SqlFile object that contains the SQL commands to run</param>
        /// <exception cref="SqlFileRunningException">If any error occurs when running the SQL commands.</exception>
        void Run(SqlFile sqlFile);
    }
}