using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace SqlRunner.Core
{
    /// <summary>
    /// Represents a SqlRunner execution mode.
    /// It has static properties that provides immutable singletons for each available mode value.
    /// </summary>
    public sealed class Mode
    {
        /// <summary>
        /// Normal mode value.
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "This type is immutable.")] public static readonly Mode Normal = new Mode("Normal", true, true);

        /// <summary>
        /// UpdateVersionOnly mode value.
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "This type is immutable.")]
        public static readonly Mode UpdateVersionOnly = new Mode("UpdateVersionOnly", true, false);

        /// <summary>
        /// Simulation mode value.
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "This type is immutable.")]
        public static readonly Mode Simulation = new Mode("Simulation", false, false);

        /// <summary>
        /// All mode values.
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes", Justification = "This type is immutable.")]
        public static readonly IEnumerable<Mode> Values = new[] { Normal, UpdateVersionOnly, Simulation };

        private static readonly Dictionary<string, Mode> Modes = Values.ToDictionary(x => x.Name);
        private readonly bool _executeSql;


        private readonly string _name;

        private readonly bool _updateVersions;

        private Mode(string name, bool updateVersions, bool executeSql) {
            _name = name;
            _updateVersions = updateVersions;
            _executeSql = executeSql;
        }

        /// <summary>
        /// Mode's name
        /// </summary>
        public string Name {
            get { return _name; }
        }

        /// <summary>
        /// Does this mode assimilate the versions of the new/changed SQL files?
        /// If not, a SQL file detected as new/changed during the current execution (and, eventually having its SQL script executed), will be detected again as new/changed in a future execution.
        /// </summary>
        public bool UpdateVersions
        {
            get { return _updateVersions; }
        }

        /// <summary>
        /// Does this mode execute the SQL script contained in new/changed files?
        /// If not, the SQL script will be ignored during the current execution.
        /// </summary>
        public bool ExecuteSql
        {
            get { return _executeSql; }
        }

        /// <summary>
        /// Try to parse a mode instance from its name.
        /// </summary>
        /// <param name="modeName">name of the mode</param>
        /// <param name="mode">parsed mode</param>
        /// <returns>true if parsing was successfull</returns>
        public static bool TryParse(string modeName, out Mode mode) {
            return Modes.TryGetValue(modeName, out mode);
        }

        public override string ToString() {
            return Name;
        }
    }
}