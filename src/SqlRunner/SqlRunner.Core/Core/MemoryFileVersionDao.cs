using System.Collections.Generic;

namespace SqlRunner.Core
{
    /// <summary>
    /// Implementation of <see cref="IFileVersionDao"/> that uses the RAM memory as the persistence storage.
    /// </summary>
    public class MemoryFileVersionDao : IFileVersionDao
    {
        private readonly IDictionary<string, string> _fileVersions = new Dictionary<string, string>();

        protected IDictionary<string, string> FileVersions {
            get { return _fileVersions; }
        }

        #region IFileVersionDao Members

        public string Find(string fileId) {
            string version = _fileVersions.TryGetValue(fileId, out version) ? version : null;
            Logger.Log(this, "Reading version of file '{0}': '{1}'".FormatSelf(fileId, version));
            return version;
        }

        public virtual void Save(string fileId, string version) {
            Logger.Log(this, "Saving version of file '{0}': '{1}'".FormatSelf(fileId, version));
            SaveInternal(fileId, version);
        }

        #endregion

        /// <summary>
        /// Persists in RAM (transient) memory a file's version
        /// </summary>
        /// <param name="fileId">file's identifier</param>
        /// <param name="version">file's version</param>
        private void SaveInternal(string fileId, string version) {
            _fileVersions[fileId] = version;
            PersistData();
        }

        /// <summary>
        /// Persists in a persistent (non-transient) storage a file's version
        /// </summary>
        protected virtual void PersistData() {}
    }
}