using System;

namespace SqlRunner.Core
{
    public interface ISqlFileVersionManager
    {
        Func<SqlFile, string> HashCalculator { get; }
        bool FileVersionIsOutdated(SqlFile sqlFile);
        void Save(SqlFile file);
    }
}