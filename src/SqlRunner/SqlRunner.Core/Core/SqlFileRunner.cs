using System;
using System.Data;
using System.Data.Common;
using System.IO;

namespace SqlRunner.Core
{
    /// <summary>
    /// Implementation of <see cref="ISqlFileRunner"/> that runs against a database using a connection string and a data provider.
    /// </summary>
    public class SqlFileRunner : ISqlFileRunner
    {
        private DbConnection _connection;
        private bool _disposed;

        /// <summary>
        /// Create an instance that runs against a database using the specified connection string and data provider.
        /// </summary>
        /// <param name="connectionString">connection string</param>
        /// <param name="dataProviderName">data provider's name</param>
        public SqlFileRunner(string connectionString, string dataProviderName) {
            SqlFileReader = new SqlFileReader();

            Logger.Log(this, "Preparing to use data provider '{0}'.".FormatSelf(dataProviderName));
            var dbProviderFactory = DbProviderFactories.GetFactory(dataProviderName);
            _connection = dbProviderFactory.CreateConnection();
            // ReSharper disable PossibleNullReferenceException
            _connection.ConnectionString = connectionString;
            // ReSharper restore PossibleNullReferenceException
        }

        internal ISqlFileReader SqlFileReader { private get; set; }

        #region ISqlFileRunner Members

        public void Run(SqlFile sqlFile) {
            if (_disposed) {
                throw new ObjectDisposedException(GetType().Name);
            }
            if (sqlFile == null) {
                throw new ArgumentNullException("sqlFile");
            }

            Func<Exception, SqlFileRunningException> logAndCreateException = delegate(Exception exception) {
                Logger.Log(this, "Error running SQL file '{0}':{1}".FormatSelf(sqlFile.Info.FullName, exception));
                return new SqlFileRunningException("Error running SQL file '{0}'".FormatSelf(sqlFile.Info.FullName),exception);
            };

            try {
                if (_connection.State != ConnectionState.Open) {
                    Logger.Log(this, "Connecting to database using connection string '{0}'.".FormatSelf(_connection.ConnectionString));
                    _connection.Open();
                }

                Logger.Log(this, "Running SQL file '{0}'".FormatSelf(sqlFile.Info.FullName));
                using (var sqlFileInputStream = sqlFile.Info.OpenRead()) {
                    SqlFileReader.ReadRunningSqlCommands(sqlFileInputStream, sqlFile.Info.Length, RunSqlCommand);
                }
            }
            catch (UnauthorizedAccessException e) {
                throw logAndCreateException(e);
            }
            catch (IOException e) {
                throw logAndCreateException(e);
            }
            catch (DbException e) {
                throw logAndCreateException(e);
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        private void RunSqlCommand(string sqlCommand, long processedBytes, long totalBytes) {
            using (var command = _connection.CreateCommand()) {
                command.CommandText = sqlCommand;
                Logger.Log(this, "Processed {0} of {1} bytes. Running SQL: {2}".FormatSelf(processedBytes, totalBytes, sqlCommand));
                try {
                    command.ExecuteNonQuery();
                }
                catch (Exception e) {
                    Logger.Log(this, "Error running SQL: {0}".FormatSelf(e));
                    throw;
                }
            }
        }

        protected virtual void Dispose(bool disposing) {
            if (_disposed) {
                return;
            }
            if (disposing) {
                if (_connection != null) {
                    _connection.Dispose();
                    _connection = null;
                }
            }
            _disposed = true;
        }
    }
}