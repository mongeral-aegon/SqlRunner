﻿namespace SqlRunner.Core
{
    /// <summary>
    /// Saves and retrieves file versions from the persistence infrastructure.
    /// </summary>
    public interface IFileVersionDao
    {
        /// <summary>
        /// Retrieve a persisted file's version, or null when the file is unknown.
        /// </summary>
        /// <param name="fileId">identifier of the file</param>
        /// <returns>file's version</returns>
        string Find(string fileId);

        /// <summary>
        /// Persists a file's version
        /// </summary>
        /// <param name="fileId">Ffile's identifier</param>
        /// <param name="version">file's version</param>
        void Save(string fileId, string version);
    }
}