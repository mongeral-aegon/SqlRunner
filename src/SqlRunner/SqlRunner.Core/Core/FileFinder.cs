using System;
using System.Collections.Generic;
using System.IO;

namespace SqlRunner.Core
{
    /// <summary>
    /// Implementation of <see cref="IFileFinder"/>, that searches files in a set of predefined directories that matches a specified mask that can contain wildcard characters.
    /// </summary>
    public class FileFinder : IFileFinder
    {
        private readonly IEnumerable<string> _directories;
        private readonly string _pattern;

        /// <summary>
        /// Creates an instance that searches files in a set of predefined directories that matches a specified mask.
        /// </summary>
        /// <param name="directories">list of directories in which the files will be searched</param>
        /// <param name="pattern">mask, that can contain wildcard characters, which the searched files must match. If ommited, "*" will be used.</param>
        public FileFinder(IEnumerable<string> directories, string pattern = "*") {
            if (directories == null) {
                throw new ArgumentNullException("directories");
            }
            if (pattern == null) {
                throw new ArgumentNullException("pattern");
            }

            _directories = directories;
            _pattern = pattern;
        }

        #region IFileFinder Members

        public IEnumerable<string> Find() {
            var files = new HashSet<string>();
            foreach (string directory in _directories) {
                var directoryInfo = new DirectoryInfo(directory);
                if (directoryInfo.Exists) {
                    foreach (var file in directoryInfo.EnumerateFiles(_pattern, SearchOption.AllDirectories)) {
                        files.Add(file.FullName);
                    }
                }
            }
            return files;
        }

        #endregion
    }
}