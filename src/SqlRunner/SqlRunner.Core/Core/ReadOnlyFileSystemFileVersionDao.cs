namespace SqlRunner.Core
{
    /// <summary>
    /// A modified implementation of <see cref="FileSystemFileVersionDao"/> that is read-only, just logging, instead of executing, calls to the <see cref="IFileVersionDao.Save">Save</see> method.
    /// </summary>
    public class ReadOnlyFileSystemFileVersionDao : FileSystemFileVersionDao
    {
        public ReadOnlyFileSystemFileVersionDao(string controlFilePath) : base(controlFilePath) {}

        public override void Save(string fileId, string version) {
            Logger.Log(this, "[SKIPPED] Saving version of file '{0}': '{1}'".FormatSelf(fileId, version));
        }
    }
}