using System;

namespace SqlRunner.Core
{
    public sealed class SimulationSqlFileRunner : ISqlFileRunner
    {
        #region ISqlFileRunner Members

        public void Run(SqlFile sqlFile) {
            if (sqlFile == null) {
                throw new ArgumentNullException("sqlFile");
            }
            Logger.Log(this, "[SKIPPED] Running SQL file '{0}'".FormatSelf(sqlFile.Info.FullName));
        }

        public void Dispose() {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}