namespace SqlRunner.Core
{
    public class DataItem
    {
        public DataItem() {}

        public DataItem(string key, string value) {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }
        public string Value { get; set; }
    }
}