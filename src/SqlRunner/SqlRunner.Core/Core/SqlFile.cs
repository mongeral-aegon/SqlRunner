using System;
using System.IO;

namespace SqlRunner.Core
{
    /// <summary>
    /// Represents a SQL file and is capable to compute a hash of the file's contents.
    /// </summary>
    public class SqlFile
    {
        private readonly FileInfo _fileInfo;
        private readonly Func<SqlFile, string> _hashCalculator;
        private string _cachedHash;

        public SqlFile(string filePath) : this(filePath, DefaultHashCalculation) {}

        public SqlFile(string filePath, Func<SqlFile, string> hashCalculator) {
            if (filePath == null) {
                throw new ArgumentNullException("filePath");
            }
            if (hashCalculator == null) {
                throw new ArgumentNullException("hashCalculator");
            }
            _hashCalculator = hashCalculator;
            _fileInfo = new FileInfo(filePath);
        }

        /// <summary>
        /// SQL file represented by this instance.
        /// </summary>
        public FileInfo Info {
            get { return _fileInfo; }
        }

        /// <summary>
        /// Returns the computed hash of the SQL file's contents. Note that the hash is computed only on the first call, being cached and returned in subsequent calls.
        /// </summary>
        public string Hash {
            get {
                _cachedHash = _cachedHash ?? _hashCalculator(this);
                return _cachedHash;
            }
        }

        private static string DefaultHashCalculation(SqlFile sqlFile) {
            return sqlFile.Info.Exists ? new FileInfo(sqlFile.Info.FullName).ContentsHash() : string.Empty;
        }
    }
}