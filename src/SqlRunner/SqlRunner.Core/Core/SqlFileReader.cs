using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;

namespace SqlRunner.Core
{
    public class SqlFileReader : ISqlFileReader
    {
        #region ISqlFileReader Members

        [SuppressMessage("Gendarme.Rules.Performance", "AvoidRepetitiveCallsToPropertiesRule",
            Justification = "A different value is expected from each call to the property stream.Position.")]
        public void ReadRunningSqlCommands(Stream stream, long streamTotalBytes, Action<string, long, long> sqlExecutor) {
            if (stream == null) {
                throw new ArgumentNullException("stream");
            }
            if (sqlExecutor == null) {
                throw new ArgumentNullException("sqlExecutor");
            }
            using (var reader = new StreamReader(stream, Encoding.GetEncoding(1252))) {
                var isInsideString = false;
                var sqlCommand = new StringBuilder();
                while (reader.Peek() >= 0) {
                    var character = (char) reader.Read();
                    if (character == '\'') {
                        isInsideString = !isInsideString;
                        sqlCommand.Append(character);
                    } else {
                        if (character == ';' && !isInsideString) {
                            RunSql(sqlCommand, sqlExecutor, stream.Position, streamTotalBytes);
                        } else {
                            sqlCommand.Append(character);
                        }
                    }
                }
                RunSql(sqlCommand, sqlExecutor, stream.Position, streamTotalBytes);
            }
        }

        #endregion

        private static void RunSql(StringBuilder sqlCommandBuilder, Action<string, long, long> sqlExecutor, long processedBytes, long totalBytes) {
            var sqlCommandText = sqlCommandBuilder.ToString().Trim();
            sqlCommandBuilder.Clear();
            if (!String.IsNullOrEmpty(sqlCommandText)) {
                sqlExecutor(sqlCommandText, processedBytes, totalBytes);
            }
        }
    }
}