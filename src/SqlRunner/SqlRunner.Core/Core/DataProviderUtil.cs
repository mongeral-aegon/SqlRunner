﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace SqlRunner.Core
{
    public static class DataProviderUtil
    {
        public static IList<SupportedDataProvider> ListSupported() {
            using (var supportedDataProvidersDataTable = DbProviderFactories.GetFactoryClasses()) {
                return (from object row in supportedDataProvidersDataTable.Rows
                        let dataRow = (DataRow) row
                        select new SupportedDataProvider {
                                                             FriendlyName = dataRow[0].ToString(),
                                                             InvariantName = dataRow[2].ToString()
                                                         }).ToList();
            }
        }

        public static bool IsSupported(string dataProviderName) {
            try {
                DbProviderFactories.GetFactory(dataProviderName);
                return true;
            }
            catch (ArgumentException) {
                return false;
            }
        }
    }
}