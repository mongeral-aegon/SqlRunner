﻿using System;

namespace SqlRunner.Core
{
    public class LoggedEventArgs : EventArgs
    {
        public LoggedEventArgs(string message) {
            Message = message;
        }

        public string Message { get; private set; }
    }
}