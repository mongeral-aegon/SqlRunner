﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.RegularExpressions;
using SqlRunner.Core;

namespace SqlRunner.Console
{
    [SuppressMessage("Gendarme.Rules.BadPractice", "DisableDebuggingCodeRule",
        Justification =
            "This class represents all the functionality of the SqlRunner console application. It was extracted form the original main class, in order to reside in a .dll assembly, allowing to be referenced by the testing assembly and, thus, be testable."
        )]
    public static class SqlRunnerConsoleApp
    {
        // ReSharper disable ConvertToConstant.Global
        // ReSharper disable UnassignedReadonlyField
        private const string Help =
            "{0}Searches and runs SQL script files changed/created since last execution." +
            "{0}" +
            "{0}Usage: SqlRunner -controlFile:<path> -searchDir:<directory path>" +
            "{0}  -connectionString:<database connection string> -dataProviderName:<name>" +
            "{0}  [-mode:<mode>]" +
            "{0}" +
            "{0}Arguments:" +
            "{0}" +
            "{0}  -searchDir:   Full path of a directory containing SQL scripts to process." +
            "{0}                Can be repeated to specify multiple directories." +
            "{0}                SQL script files are searched in each specified directory," +
            "{0}                as well as in all its descendants subdirectories." +
            "{0}  -controlFile:" +
            "{0}                Path of the XML version control file." +
            "{0}  -connectionString:" +
            "{0}                Connection string to the database the SQL scripts will run" +
            "{0}                against." +
            "{0}  -dataProviderName:" +
            "{0}                Name of the .NET data provider to be used to access the" +
            "{0}                database specified by connection string." +
            "{0}                Example: -dataProviderName:System.Data.SqlClient" +
            "{0}                Installed data providers:" +
            "{0}{1}" +
            "{0}  -mode:        Define the execution mode (default is Normal). Allowed values:" +
            "{0}                " +
            "{0}                Normal:" +
            "{0}                  Searches script files; Detect outdated script files; Run each" +
            "{0}                  outdated script file; Assimilates the new version of each" +
            "{0}                  outdated file that was successfully run." +
            "{0}                  This is the default value, and is assumed when the argument" +
            "{0}                  -mode is omitted." +
            "{0}                " +
            "{0}                UpdateVersionOnly:" +
            "{0}                  Searches script files; Detect outdated script files; " +
            "{0}                  Assimilates the new version of each outdated file." +
            "{0}                  Tipically used in the first SqlRunner run, or when some files" +
            "{0}                  were added and already executed outside SqlRunner." +
            "{0}" +
            "{0}                Simulation:" +
            "{0}                  Searches script files; Detect outdated script files." +
            "{0}" +
            "{0}Example:" +
            "{0}" +
            "{0}SqlRunner -searchDir:c:\\work\\MyProject -controlFile:c:\\work\\controlfile.xml \"-connectionString:Data Source=C:\\WORK\\MySqlCeDatabase.sdf\\\" -dataProviderName:System.Data.SqlServerCe.4.0" +
            "{0}";

        [SuppressMessage("Gendarme.Rules.Performance", "PreferLiteralOverInitOnlyFieldsRule",
            Justification =
                "This field is being referenced in another assembly. If it was a constant, the field would be replaced by the constant values, so, value changing would required recompilation of referencing assemblies."
            )] internal static readonly byte SuccessExitCode;

        // ReSharper restore UnassignedReadonlyField

        [SuppressMessage("Gendarme.Rules.Performance", "PreferLiteralOverInitOnlyFieldsRule",
            Justification =
                "This field is being referenced in another assembly. If it was a constant, the field would be replaced by the constant values, so, value changing would required recompilation of referencing assemblies."
            )] internal static readonly byte ErrorExitCode = 255;

        // ReSharper restore ConvertToConstant.Global

        private static readonly Regex ArgumentRegex = new Regex(@"-([^:]*)\:?(.*)");
        private static readonly List<string> SearchDirectories = new List<string>();
        private static string _dataProviderName;
        private static string _connectionString;
        private static string _controlFilePath;
        private static string _specifiedMode;
        private static Mode _mode;

        [SuppressMessage("Gendarme.Rules.Correctness", "EnsureLocalDisposalRule", Justification = "The using block disposes sqlFileRunner object.")]
        public static byte AppMain(string[] args) {
            Initialize();
            if (args == null || args.Length == 0) {
                System.Console.WriteLine(Help.FormatSelf(Environment.NewLine, ListSupportedDataProviders(16)));
                return SuccessExitCode;
            }
            foreach (var arg in args) {
                ReadArgument(arg);
            }
            if (ValidateArguments()) {
                Logger.Log(typeof (SqlRunnerConsoleApp),
                           "The mode '{0}' is activated.{1}{2}".FormatSelf(_mode,
                                                                           _mode.UpdateVersions
                                                                               ? String.Empty
                                                                               : " *** No file versions will be assimilated. ***",
                                                                           _mode.ExecuteSql ? String.Empty : " *** No SQL scripts will be run. ***"));
                IFileVersionDao fileVersionDao = _mode.UpdateVersions
                                                     ? new FileSystemFileVersionDao(_controlFilePath)
                                                     : new ReadOnlyFileSystemFileVersionDao(_controlFilePath);
                IFileFinder fileFinder = new FileFinder(SearchDirectories, "*.sql");
                ISqlFileRunner sqlFileRunner = _mode.ExecuteSql
                                                   ? (ISqlFileRunner) new SqlFileRunner(_connectionString, _dataProviderName)
                                                   : new SimulationSqlFileRunner();
                using (sqlFileRunner) {
                    var sqlFileVersionManager = new SqlFileVersionManager(fileVersionDao);
                    using (var sqlRunnerManager = new SqlRunnerManager(sqlFileVersionManager, fileFinder, sqlFileRunner)) {
                        sqlRunnerManager.Run();
                        return SuccessExitCode;
                    }
                }
            }
            return ErrorExitCode;
        }

        private static void Initialize() {
            SearchDirectories.Clear();
            _dataProviderName = _connectionString = _controlFilePath = _specifiedMode = null;
            _mode = Mode.Normal;
            Logger.ClearEventHandlers();
            Logger.Logged +=
                (sender, loggerEventArgs) =>
                System.Console.WriteLine("[SqlRunner][{0} {1}] {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), loggerEventArgs.Message);
        }

        private static void ReadArgument(string argument) {
            string parameterName;
            string parameterValue;
            if (!ParseArgument(argument, out parameterName, out parameterValue)) {
                return;
            }
            Logger.Log(typeof (SqlRunnerConsoleApp), "Reading command-line argument '{0}' with value '{1}'".FormatSelf(parameterName, parameterValue));
            switch (parameterName) {
                case "controlFile":
                    _controlFilePath = parameterValue;
                    break;
                case "searchDir":
                    SearchDirectories.Add(parameterValue);
                    break;
                case "connectionString":
                    _connectionString = parameterValue;
                    break;
                case "dataProviderName":
                    _dataProviderName = parameterValue;
                    break;
                case "mode":
                    _specifiedMode = parameterValue;
                    break;
            }
        }

        [SuppressMessage("Gendarme.Rules.Performance", "AvoidRepetitiveCallsToPropertiesRule",
            Justification = "The calls to property Value seem to be repetitive, but are being called in different Group objects, returning different values.")]
        private static bool ParseArgument(string argument, out string parameterName, out string parameterValue) {
            var matchGroups = ArgumentRegex.Match(argument).Groups;
            if (matchGroups.Count < 3) {
                parameterName = parameterValue = string.Empty;
                return false;
            }
            parameterValue = matchGroups[2].Value;
            parameterName = matchGroups[1].Value;
            return true;
        }

        private static bool ValidateArguments() {
            var sb = new StringBuilder();
            if (SearchDirectories.Count == 0) {
                sb.AppendLine(
                    "ERROR: No search directory was specified. Use one or more -searchDir arguments to specify directories to search.");
            }
            if (String.IsNullOrEmpty(_controlFilePath)) {
                sb.AppendLine(
                    "ERROR: No XML control file path was specified. Use the -controlFile argument to specify it.");
            }
            if (_specifiedMode != null) {
                if (!Mode.TryParse(_specifiedMode, out _mode)) {
                    _mode = Mode.Normal;
                    sb.AppendLine(
                        "ERROR: The specified mode '{0}' was not recognized. Allowed values are: {1}".FormatSelf(_specifiedMode, string.Join(", ", Mode.Values)));
                }
            }

            if (_mode.ExecuteSql) {
                if (String.IsNullOrEmpty(_connectionString)) {
                    sb.AppendLine(
                        "ERROR: No connection string was specified. Use the -connectionString argument to specify it.");
                }
                if (String.IsNullOrEmpty(_dataProviderName)) {
                    sb.AppendLine(
                        "ERROR: No data provider name was specified. Use the -dataProviderName argument to specify it.");
                } else if (!DataProviderUtil.IsSupported(_dataProviderName)) {
                    sb.AppendLine(
                        "ERROR: The specified data provider '{0}' is not supported. Change the -dataProviderName argument to specify one of the following installed data providers: {1}"
                            .FormatSelf(_dataProviderName, ListSupportedDataProviders(7)));
                }
            }
            System.Console.Write(sb);
            return sb.Length == 0;
        }

        private static string ListSupportedDataProviders(int leftMargin) {
            var sb = new StringBuilder();
            foreach (SupportedDataProvider supportedDataProvider in DataProviderUtil.ListSupported()) {
                sb.AppendLine("{0}'{1}' ({2})".FormatSelf(new string(' ', leftMargin), supportedDataProvider.InvariantName,
                                                          supportedDataProvider.FriendlyName.Replace(" Data Provider", String.Empty)));
            }
            return sb.ToString();
        }
    }
}