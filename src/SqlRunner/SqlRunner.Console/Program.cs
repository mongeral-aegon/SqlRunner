﻿using System;

namespace SqlRunner.Console
{
    internal static class Program
    {
        public static void Main(string[] args) {
            Environment.Exit(SqlRunnerConsoleApp.AppMain(args));
        }
    }
}